# Stashcat API "client"

This was written while taking a closer look at "Hermine" the Messenger of the
German Federal Agency for Technical Relief (THW).

## Prepare the Environment

``` 
python3 -m venv .venv
source .venv/bin/activate
pip3 install -r requirements.txt
```
